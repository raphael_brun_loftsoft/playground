package ch.inftec.demo.esw.model;

/**
 * Created with IntelliJ IDEA.
 * User: rotscher
 * Date: 10.10.13
 * Time: 13:50
 * To change this template use File | Settings | File Templates.
 */
public class Router {

    private String name;
    private String ipAddress;

    
    public Router(String name, String ipAddress) {
		this.name = name;
		this.ipAddress = ipAddress;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
